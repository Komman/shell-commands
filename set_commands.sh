#!/bin/bash

cd "bin"

coms=$(ls)

for c in $coms
do
	sudo cp "$c" "/usr/bin/$c"
done

cd ../man

coms=$(ls)

for c in $coms
do
	sudo cp "$c" "/usr/share/man/man1/$c"
done