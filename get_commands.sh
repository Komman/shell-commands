#!/bin/bash

COMMAND_FILE="./commands.txt"

com=$(cat $COMMAND_FILE)

for c in $com
do
	sudo cp "/usr/bin/$c" "./bin/"
	sudo cp "/usr/share/man/man1/$c.1.gz" "./man/"
done
